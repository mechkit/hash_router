export default function(callback){

  function hash_check(callback) {
    if( window.location.hash === '' || window.location.hash === '#' || window.location.hash === '#/' ){
      callback(false);
    } else {
      var url = location.hash.slice(2) || '/';
      var values = url.split('/');
      callback(values);
    }
  }

  if ( callback ){
    // Listen on hash change:
    window.addEventListener('hashchange', function(){
      hash_check(callback);
    });
    // Listen on page load:
    //window.addEventListener('load', function(){
    //  hash_check(callback);
    //});
  }

  return function(new_route){
    if( new_route && new_route.constructor === String ){
      window.location.hash = '#/' + new_route;
    } else {
      hash_check(callback);
    }
  };

}
